﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Cells<T> : MonoBehaviour where T : MonoBehaviour
{
    protected int rowCount;
    protected int columnCount;
    protected int cellSize;

    protected T[,] cells;
    private Vector2 offset;

    public Cells(RectTransform field, T cellPrefab)
    {
        int width = (int)field.rect.width;
        int height = (int)field.rect.height;

        cellSize = (int)cellPrefab.GetComponent<RectTransform>().rect.width;
        int cellSizeHalf = (int)cellSize / 2;

        int worldWightHalf = (int)width / 2;
        int worlfHeightHalf = (int)height / 2;
        offset = new Vector2(-worldWightHalf + cellSizeHalf, worlfHeightHalf - cellSizeHalf);
    }

    public abstract void CreateCells(RectTransform field);

    public T[] this[int x]
    {
        get
        {
            int columnCount = cells.GetLength(1);

            T[] row = new T[columnCount];

            for(int i = 0; i < columnCount; i++)
            {
                row[i] = cells[x, i];
            }

            return row;
        }
    }

    public T this[Point point]
    {
        get
        {
            return cells[point.x, point.y];
        }
        set
        {
            cells[point.x, point.y] = value;
        }
    }

    public Vector2 GetPosition(int x, int y)
    {
        return offset + new Vector2(x * cellSize, -y * cellSize);
    }

    public Vector2 GetPosition(Point point)
    {
        return offset + new Vector2(point.x * cellSize, -point.y * cellSize);
    }

    public T GetCell(int x, int y)
    {
        return cells[x, y];
    }

    public void SetCell(int x, int y, T cell)
    {
        cells[x, y] = cell;
    }    

    public int GetColumnCount()
    {
        return columnCount;
    }

    public int GetRowCount()
    {
        return rowCount;
    }
}
