﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prefabs : Singleton<Prefabs>
{
    public Cell Cell;
    public BattleCell battleCell;
    public Unit Unit;
    public Enemy Enemy;
}