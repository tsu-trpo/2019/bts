﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResStorage : Singleton<ResStorage>
{
    protected override void Init()
    {
        waterCell = Resources.Load<Sprite>("WorldScene/Cells/Water");
        grassCell = Resources.Load<Sprite>("WorldScene/Cells/Grass");
        fogCell = Resources.Load<Sprite>("WorldScene/Cells/Fog");

        player = Resources.Load<Sprite>("WorldScene/Player/Heroi");
        playerUp = Resources.Load<Sprite>("WorldScene/Player/Heroi_up");
        playerDown = Resources.Load<Sprite>("WorldScene/Player/Heroi_down");
    }

    public Sprite waterCell;
    public Sprite grassCell;
    public Sprite fogCell;

    public Sprite player;
    public Sprite playerUp;
    public Sprite playerDown;
}
