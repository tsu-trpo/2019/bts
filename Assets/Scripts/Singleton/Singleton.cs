﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T>: MonoBehaviour where T: MonoBehaviour
{
    private static T instance;
    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Warning! Dublicate");
            DestroyImmediate(gameObject);
            return;
        }

        Init();

        DontDestroyOnLoad(gameObject);
        instance = GetComponent<T>();
    }

    protected virtual void Init(){}
    public static T Get()
    {
        return instance;
    }
}
