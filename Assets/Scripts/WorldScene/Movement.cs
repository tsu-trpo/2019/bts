﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement
{
    PlayerController player;

    public Movement(PlayerController player)
    {
        this.player = player;
    }

    public void GoTo(Cell cell)
    {
        player.StartMove(cell);
    }
}
