﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    WorldCells cells;
    
    public EnemySpawner(Transform world, WorldCells cells, List<Squad> squads)
    {
        this.cells = cells;

        SpawnUnit(world, squads);
    }

    private void SpawnUnit(Transform world, List<Squad> squads)
    {
        for(int i = 0; i < squads.Count; i++)
        {
            Enemy enemy = Instantiate<Enemy>(Prefabs.Get().Enemy);

            enemy.transform.SetParent(world.transform);
            enemy.Init(cells, squads[i]);
        }
    }
}
