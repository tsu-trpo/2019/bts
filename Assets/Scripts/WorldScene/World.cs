﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class World : MonoBehaviour
{
    public PlayerController player;

    void Start()
    {
        RectTransform worldRect = GetComponent<RectTransform>();

        Movement movement = new Movement(player);

        string name = "Map.txt";
        Parser parser = new Parser(name);

        List<List<int>> map = parser.GetMap();
        WorldCells cells = new WorldCells(worldRect, movement, map);

        Squad playerSquad = parser.GetPlayerSquad();
        player.Init(cells, playerSquad);

        List<Squad> enemiesSquads = parser.GetEnemiesSquads();
        EnemySpawner enemySpawner = new EnemySpawner(transform, cells, enemiesSquads);
    }
}