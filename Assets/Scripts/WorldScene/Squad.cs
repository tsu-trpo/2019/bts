﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Squad
{
    public Point point;
    public List<UnitInfo> units;

    public Squad(Point point, List<UnitInfo> units)
    {
        this.point = point;
        this.units = units;
    }
}
