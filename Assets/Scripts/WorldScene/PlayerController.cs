﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
	public int visibility = 2;
	
	bool running = false;
	float movingDuration = 0.5f;

	List<UnitInfo> units;
	WorldCells cells;
	BuildWave buildWave;
	SpriteRenderer spriteRenderer;

	private Cell _currentCell;
	public Cell currentCell 
	{
		get
		{
			return _currentCell;
		}

		set 
		{
			if(_currentCell != value) {
				ChangeSprite(_currentCell, value);
				
				Vector3 target = value.transform.position + OffsetConsts.player;
				Move(target);
				
				_currentCell = value;

				OpenCell();
			}
		}
	}

	public void Init(WorldCells cells, Squad squad)
	{
		this.cells = cells;
		this.units = squad.units;

		spriteRenderer = GetComponent<SpriteRenderer>();

		currentCell = cells[squad.point];
		
		buildWave = new BuildWave(cells);
	}

	public IEnumerator Move(Cell cell)
	{
		List<Cell> path = buildWave.CreatePath(currentCell, cell);

		if(cell.Interact != null)
		{
			path.RemoveAt(path.Count - 1);
		}

		foreach(Cell _cell in path)
		{
			currentCell = _cell;
			OpenCell();
			yield return new WaitForSeconds(movingDuration);
		}

		running = false;

		if(cell.Interact != null)
		{
			cell.Interact();
		}
	}

	public void StartMove(Cell cell)
	{
		if(!running)
		{
			running = true;
			StartCoroutine(Move(cell));
		}
	}

	void ChangeSprite(Cell previousCell, Cell nextCell)
	{
		if(previousCell == null)
		{
			return;
		}

		if(previousCell.point.y == nextCell.point.y)
		{
			spriteRenderer.flipX = previousCell.point.x > nextCell.point.x;
			spriteRenderer.sprite = ResStorage.Get().player;
		
		}
		else if(previousCell.point.x == nextCell.point.x)
		{
			spriteRenderer.sprite = (previousCell.point.y < nextCell.point.y) ?
                                                ResStorage.Get().playerDown :
                                                ResStorage.Get().playerUp;
		}
	}

	void Move(Vector3 target)
	{
		if(_currentCell == null)
		{
			transform.position = target;
		}
		else
		{
			transform.DOMove(target, movingDuration, false);
		}
	}

	void OpenCell()
	{
        	List<Cell> visibleCells = cells.GetVisibleCells(currentCell.point, visibility);

		foreach(Cell cell in visibleCells) 
		{
			cell.fog = false;
		}
    	}

	float GetHeight()
	{
		return transform.localScale.y;
	}

	public int GetX() {
		return currentCell.point.x;
	}

	public int GetY() {
		return currentCell.point.y;
	}
}