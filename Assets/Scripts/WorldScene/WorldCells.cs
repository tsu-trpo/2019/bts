﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class WorldCells : Cells<Cell>
{
    private List<List<int>> map;
    private Movement movement;

    public WorldCells(RectTransform field, Movement movement, List<List<int>> map) : base(field, Prefabs.Get().Cell) 
    {
        this.movement = movement;
        this.map = map;

        CreateCells(field);
    }

    public override void CreateCells(RectTransform field)
    {
        rowCount = map.Count;
        columnCount = map[0].Count;

        Cell.Type type;
        cells = new Cell[columnCount, rowCount];

        for(int i = 0; i < rowCount; i++)
        {
            for(int j = 0; j < columnCount; j++)
            {
                Cell cell = Instantiate<Cell>(Prefabs.Get().Cell);

                cell.transform.SetParent(field.transform);

                bool isType = Enum.IsDefined(typeof(Cell.Type), map[i][j]);
                if(isType)
                {
                    type = (Cell.Type)map[i][j];
                }
                else
                {
                    Debug.LogWarning("Ошибка! Некоректный файл с картой");
                    break;
                }

                Point point = new Point(j, i);
                cell.Init(point, type, movement);

                cell.transform.localPosition = GetPosition(cell.point);

                this[point] = cell;
            }
        }

        PostInit();
    }

    private int[] ReadWorldFromFile(string name)
    {
        string path = "Assets/Resources/WorldScene/Text/" + name;
        string line = " ";

        StreamReader reader = new StreamReader(path);

        while(!reader.EndOfStream)
        {
            rowCount++;
            line += reader.ReadLine() + ' ';
        }

        int[] map = line.Trim().Split(' ').Select(n => int.Parse(n)).ToArray();
        columnCount = (int)(map.Length / rowCount);

        return map;
    }

    private void PostInit()
    {
        foreach(Cell cell in cells)
        {
            cell.BordersInit(cells);
        }
    }

    public List<Cell> GetVisibleCells(Point point, int visibility)
    {
        List<Cell> visibleCells = new List<Cell>();
        
        Action<int, int> TryAdd = (x, y) =>
        {
            bool outside = ((x < 0) || (columnCount - 1 < x) || (y < 0) || (rowCount - 1 < y));

            if(!outside)
            {
                visibleCells.Add(cells[x, y]);
            }
        };

        int xUpBorder = point.x + visibility;
        int yUpBorder = point.y + visibility;
        int xDownBorder = point.x - visibility;
        int yDownBorder = point.y - visibility;
        
        for(int i = xDownBorder; i <= xUpBorder; i++)
        {
            for(int j = yDownBorder; j <= yUpBorder; j++)
            {
                TryAdd(i, j);
            }
        }

        return visibleCells;
    }
}