﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public World world;

    float speed = 105;
    float speedRatio;
    const float sensitivity = 5f;

    float aspect;

    float upBorder;
    float downBorder;
    float leftBorder;
    float rightBorder;

    int minSize;
    int maxSize;

    void Start() 
    {
        //Initialization aspect and borders
        Vector2 worldPosition = world.transform.position;
        var worldRect = world.GetComponent<RectTransform>().rect;
        float worldHeightHalf = worldRect.height / 2;
        float worldWidthHalf = worldRect.width / 2;

        aspect = (float)Screen.width / (float)Screen.height; 

        var cameraSize = GetComponent<Camera>().orthographicSize;
        upBorder = worldPosition.y + worldHeightHalf - cameraSize;
        downBorder = worldPosition.y - worldHeightHalf + cameraSize;
        leftBorder = worldPosition.x - worldWidthHalf + cameraSize * aspect;
        rightBorder = worldPosition.x + worldWidthHalf - cameraSize * aspect;  

        speedRatio = speed / cameraSize;

        //Initialization minSize and maxSize
        int cellWidth = Cell.GetWidth();
        int maxCellCount = 16; 
        int minCellCount = 4; 

        minSize = minCellCount * cellWidth / 2;
        maxSize = maxCellCount * cellWidth / 2;
    }
    
    void Update()
    {
        //Camera scale
        var scroll = -10 * Input.GetAxis("Mouse ScrollWheel");

        var camerSize = GetComponent<Camera>().orthographicSize;
        var camera = GetComponent<Camera>();

        if(scroll != 0)
        {
            camera.orthographicSize = Mathf.Clamp(camerSize + scroll, minSize, maxSize);
            var realScroll = camera.orthographicSize - camerSize;

            upBorder -= realScroll;
            downBorder += realScroll;
            rightBorder -= realScroll * aspect;
            leftBorder += realScroll * aspect;

            speed = speedRatio * camera.orthographicSize;
        }
        
        //Camera move
        Vector2 mouse = Input.mousePosition;
        float move = Time.deltaTime * speed;

        if(mouse.x < sensitivity) 
        {
            transform.position -= transform.right * move;
        } 
        else if(mouse.x > Screen.width - sensitivity) 
        {
            transform.position += transform.right * move;
        }

        if(mouse.y < sensitivity)
        {
            transform.position -= transform.up * move;
        } 
        else if(mouse.y > Screen.height - sensitivity)
        {
            transform.position += transform.up * move;
        }
        
        //If camera move more than need
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, leftBorder, rightBorder), 
                                         Mathf.Clamp(transform.position.y, downBorder, upBorder), 
                                         transform.position.z);
    }
}
