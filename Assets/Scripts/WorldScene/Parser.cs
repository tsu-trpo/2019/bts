﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parser
{
    Squad player;
    List<Squad> enemies;
    List<List<int>> map;

    public Parser(string file)
    {
        string path = "Assets/Resources/WorldScene/Text/" + file;

        StreamReader reader = new StreamReader(path);
        
        //Parsing map
        map = new List<List<int>>();

        string line = reader.ReadLine();
        while(line != "Squads")
        {
            List<int> row = ParseRow(line);
            line = reader.ReadLine();
            map.Add(row);
        }
        
        //Parsing players spawnpoint and squad
        line = reader.ReadLine();

        string[] playerInfo = line.Trim().Split(':');

        Point playerPoint = ParsePoint(playerInfo[1]);
        List<UnitInfo> playerSquad = ParseSquad(playerInfo[2]);

        player = new Squad(playerPoint, playerSquad);

        //Parsing enemies spawnpoints and squads
        enemies = new List<Squad>();
        while(!reader.EndOfStream)
        {
            line = reader.ReadLine();
            string[] enemyInfo = line.Split(':');

            Point enemyPoint = ParsePoint(enemyInfo[0]);
            List<UnitInfo> enemySquad = ParseSquad(enemyInfo[1]);

            Squad enemy = new Squad(enemyPoint, enemySquad);
            enemies.Add(enemy);
        }
    }

    List<int> ParseRow(string row)
    {
        List<int> _row = new List<int>();
        
        string[] rowElements = row.Trim().Split(' ');
        
        for(int i = 0; i < rowElements.Length; i++)
        {
            int element = int.Parse(rowElements[i]);

            _row.Add(element);
        }

        return _row;
    }
    
    Point ParsePoint(string point)
    {
        string[] coordinates = point.Trim().Split(' ');

        int x = int.Parse(coordinates[0]);
        int y = int.Parse(coordinates[1]);

        return new Point(x, y);
    }
    
    List<UnitInfo> ParseSquad(string squad)
    {
        List<UnitInfo> _squad = new List<UnitInfo>();
        
        string[] squads = squad.Trim().Split(' ');
        
        for(int i = 0; i < squads.Length; i += 2)
        {
            int capacity = int.Parse(squads[i]);
            string nameUnit = squads[i + 1];

            UnitInfo unit = new UnitInfo(capacity, nameUnit);
            _squad.Add(unit);
        }

        return _squad;
    }

    public Squad GetPlayerSquad()
    {
        return player;
    }

    public List<Squad> GetEnemiesSquads()
    {
        return enemies;
    }

    public List<List<int>> GetMap()
    {
        return map;
    }
}