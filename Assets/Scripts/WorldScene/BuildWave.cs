﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildWave
{
    WorldCells cells;

    int rowCount;
    int columnCount;
    int[,] map;
    
    public BuildWave(WorldCells cells)
    {
        this.cells = cells;

        rowCount = cells.GetRowCount();
        columnCount = cells.GetColumnCount();
        map = new int[columnCount, rowCount];
    }

    public List<Cell> CreatePath(Cell startCell, Cell finalCell)
    {   
        MakingRounds(startCell,finalCell);

        List<Cell> path = new List<Cell>();
        if(IsCellsIdentical(startCell,finalCell) || this[startCell.point] < 0)
        {
            path.Add(startCell);
            return path;
        }

        path.Add(GetNextStep(startCell));
        int count = 0;
        
        while(!IsCellsIdentical(path[count], finalCell))
        {
            count++;
            Cell previosCell = path[count - 1];
            path.Add(GetNextStep(previosCell));
        }

        return path;
    }

    int this[Point point]
    {
        get
        {
            return map[point.x, point.y];
        }
        set
        {
            map[point.x, point.y] = value;
        }
    }

    private void MakingRounds(Cell startCell, Cell finalCell)
    {
        for(int x = 0; x < columnCount; x++)
        {
            for(int y = 0; y < rowCount; y++)
            {
                if(cells[x][y].occupied || cells[x][y].Interact != null)
                {
                    map[x, y] = -2;
                }	
                else 
                {
                    map[x, y] = -1;
                }
            }
        }

        if(finalCell.occupied)
        {
            return;
        }
        
        this[finalCell.point] = 0;
        
        int step = 0;

        HashSet<Point> neighbors = new HashSet<Point>();
        HashSet<Point> neighborsNextStep = new HashSet<Point>();
        neighbors.Add(finalCell.point);

        while(this[startCell.point] < 0 && neighbors.Count > 0)
        {
            neighborsNextStep.Clear();
            foreach(Point neighbor in neighbors)
            {
                neighborsNextStep.UnionWith(GetNonMarkedNeighbors(neighbor));
            }
            
            MarkCells(neighborsNextStep, step);
            neighbors = new HashSet<Point>(neighborsNextStep);
            step++;     
        }
    }

    private void MarkCells(HashSet<Point> points, int step)
    {
        foreach(Point point in points)
        {
            if(IsPassable(point))
            {
                this[point] = step + 1;   
            }
        }
    }

    private HashSet<Point> GetNeighbors(Point point, bool marked)
    {
        HashSet<Point> neighbors = new HashSet<Point>();

        Action<int, int> TryAdd = (_x, _y) =>
        {
            bool outside = ((_x < 0) || (columnCount - 1 < _x) || (_y < 0) || (rowCount - 1 < _y));

            if(!outside)
            {
                Point _point = cells[_x][_y].point;
                bool secondCondition = marked ? IsMarked(_point) : IsPassable(_point);
                
                if(secondCondition)
                {
                    neighbors.Add(_point);
                }
            }
        };
        
        int x = point.x;
        int y = point.y;

        TryAdd(x - 1, y);
        TryAdd(x + 1, y);
        TryAdd(x, y - 1);
        TryAdd(x, y + 1);

        return neighbors;
    }

    private HashSet<Point> GetNonMarkedNeighbors(Point point)
    {
        return GetNeighbors(point, false);
    }

    private HashSet<Point> GetMarkedNeighbors(Point point)
    {
        return GetNeighbors(point, true);
    }
    
    private bool IsMarked(Point point)
    {
        return this[point] >= 0;
    }

    private bool IsPassable(Point point)
    {
        return this[point] == -1;
    }

    private Cell GetNextStep(Cell startCell)
    {
        HashSet<Point> neighbors = new HashSet<Point>(GetMarkedNeighbors(startCell.point));

        Point nextStep = neighbors.OrderBy(p => this[p]).First();

        return cells[nextStep];
    }

    static bool IsCellsIdentical(Cell currentCell, Cell finalCell)
    {
        return currentCell == finalCell;
    }
}