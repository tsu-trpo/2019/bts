﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour
{
    List<UnitInfo> units;
    WorldCells cells;

    private Point _currentCell;
    public Point currentCell
    {
        get
        {
            return _currentCell;
        }

        set 
        {
            if(_currentCell != value) {
                _currentCell = value;
                
                cells[value].Interact = Fight;

                float offset = Math.Abs(GetHeight() - Cell.GetWidth() * 2) / 4; //FIXME: offset высчитывается магических образом
                transform.localPosition = (Vector3)cells.GetPosition(currentCell) + new Vector3(0, offset, -currentCell.y + OffsetConsts.enemy);
            }
        }
    }

    public Image image;

    public void Init(WorldCells cells, Squad squad)
    {
        this.cells = cells;
        this.units = squad.units;

        currentCell = squad.point;

        image.sprite = Resources.Load<Sprite>("WorldScene/Units/" + units[0].sprite);
    }

    float GetHeight()
    {
        var imageRect = image.GetComponent<RectTransform>().rect;
        return imageRect.height;
    }

    void Fight()
    {
        SceneManager.LoadScene("BattleScene");
    }
}
