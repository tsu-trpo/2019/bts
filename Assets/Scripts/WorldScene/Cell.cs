﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour
{
    public enum Type
    {
        Water,
        Grass
    }

    public Type type;

    public GameObject fogGo;
    public Image image;
    public Movement movement;
    public Point point;

    private bool _fog = true;
    public bool fog 
    {
        get
        {
            return _fog;
        }

        set 
        {
            if(_fog != value) {
                _fog = value;
                fogGo.SetActive(_fog);
            }
        }
    }

    private Action _Interact;
    public Action Interact
    {
        get
        {
            return _Interact;
        }

        set 
        {
            if(_Interact != null && value != null) {
                Debug.LogWarning("Ошибка! Переприсвивание Interact");
            }
            _Interact = value;
        }
    }

    public bool occupied
    {
        get
        {
            return Type.Water == type;
        }
    }

    public void StartMove()
    {
        if(!_fog)
        {
            movement.GoTo(this);
        }
    }

    static public int GetWidth()
    {
        Cell cell = Prefabs.Get().Cell;

        var cellRect = cell.GetComponent<RectTransform>().rect;
        int cellWidth = (int)(cellRect.width);

        return cellWidth;
    }

    public void Init(Point point, Type type, Movement movement) 
    {
        this.movement = movement;
        this.point = point;
        this.type = type;

        switch(type)
        {
            case Type.Water:
                image.sprite = ResStorage.Get().waterCell;
                break;

            case Type.Grass: 
                image.sprite = ResStorage.Get().grassCell;
                break;

            default: 
                image.sprite = ResStorage.Get().grassCell;
                Debug.LogWarning("Ошибка! Некоректный файл с картой");
                break;
        }
    }

    public void BordersInit(Cell[,] cells)
    {
        if(type != Type.Water)
        {
            return;
        }                           

        int x = point.x;
        int y = point.y;
        
        Type[] corners = new Type[4];
        corners[0] = GetType(cells, x - 1, y - 1);
        corners[1] = GetType(cells, x + 1, y - 1);
        corners[2] = GetType(cells, x + 1, y + 1);
        corners[3] = GetType(cells, x - 1, y + 1);

        Type[] sides = new Type[4];
        sides[0] = GetType(cells, x, y - 1);
        sides[1] = GetType(cells, x + 1, y);
        sides[2] = GetType(cells, x, y + 1);                
        sides[3] = GetType(cells, x - 1, y);

        string[] samples = {"1111", "1110", "1100", "1010", "1000", "0000"};

        if(IsAllWater(corners) && IsAllWater(sides))
        {
            return;
        }

        CornersAbsorption(sides, corners);
        string _sides = ToString(sides);
        string _corners = ToString(corners);

        string pathToSprite = "WorldScene/Cells/WaterBorders/";   
        
        int shiftSidesCount = AdduceToSample(ref _sides, samples);
        int shiftCornersCount = AdduceToSample(ref _corners, samples);

        int finalShiftCount = 0;
        
        if(IsAllWater(corners))
        {  
            pathToSprite += "s" + _sides;
            finalShiftCount = shiftSidesCount;
        }
        else if(IsAllWater(sides))
        {
            pathToSprite += "c" + _corners;
            finalShiftCount = shiftCornersCount;
        }
        else
        {
            while(shiftCornersCount > shiftSidesCount)
            {
                _corners = UnShift(_corners);
                shiftCornersCount--;
            } 
            while(shiftCornersCount < shiftSidesCount)
            {
                _corners = Shift(_corners);
                shiftCornersCount++;
            }
            
            pathToSprite += _sides + "_" + _corners;
            finalShiftCount = shiftSidesCount;
        }

        float angle = 90;

        image.sprite = Resources.Load<Sprite>(pathToSprite);
        transform.Rotate(0, 0, angle * (4 - finalShiftCount), Space.Self);
    }

    static bool IsAllWater(Type[] array)
    {
        foreach(Type element in array)
        {
            if(element != Type.Water)
            {
                return false;
            }
        }

        return true;
    }

    static bool CornersAbsorption(Type[] sides, Type[] corners)
    {
        for(int i = 0; i < sides.Length; i++)
        {
            if(sides[i] != Type.Water)
            {
                corners[i] = Type.Water;
                corners[ (i + 1) % sides.Length ] = Type.Water;
            }
        }

        return false;
    }

    static bool IsInRange(string element, string[] array)
    {
        foreach(string str in array)
        {
            if(element == str)
            {
                return true;
            }
        }

        return false;
    }

    static string Shift(string str)
    {   
        StringBuilder copy = new StringBuilder(str);
        copy.Remove(0, 1).Append(str[0]);
        return copy.ToString();
    }

    static string UnShift(string str)
    {   
        StringBuilder copy = new StringBuilder(str);
        copy.Insert(0, str[str.Length - 1]).Remove(str.Length, 1);
        return copy.ToString();
    }

    static Type GetType(Cell[,] cells, int x, int y)
    {
        int columnCount = cells.GetLength(0);
        int rowCount = cells.GetLength(1);

        bool outside = ((x < 0) || (columnCount - 1 < x) || (y < 0) || (rowCount - 1 < y));

        return outside ? Type.Water : cells[x, y].type;
    }

    static string ToString(Type[] array)
    {
        string line = "";

        foreach(Type element in array)
        {
            line += element.ToString("d");
        }

        return line;
    }

    static int AdduceToSample(ref string line, string[] samples)
    {
        int shiftCount = 0;

        while(!IsInRange(line, samples))
        {
            line = Shift(line);
            shiftCount++; 
        }

        return shiftCount;
    }
}