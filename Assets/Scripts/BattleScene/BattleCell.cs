﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleCell : MonoBehaviour
{
    public int x;
    public int y;

    public Image image;

    Color normalColor = new Color(0, 0, 0, normalCellOpacity);
    Color onMouseColor = new Color(0, 0, 0, normalCellOpacity * 2);

    private const float normalCellOpacity = 0.15f;

    public void Init(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    private void OnMouseEnter()
    {
        image.color = onMouseColor;
    }

    private void OnMouseExit()
    {
        image.color = normalColor;
    }
}