﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitInfo
{
    public int capacity = 7;
    public string sprite = "squire";
    
    public UnitInfo (int capacity, string sprite)
    {
        this.capacity = capacity;
        this.sprite = sprite;
    }
}