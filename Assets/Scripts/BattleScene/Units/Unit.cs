using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Unit : MonoBehaviour
{
    public int capacity = 7;
    public Text capacityFrame;

    public Image image;

    BattleCells cells;
    BattleCell unitPosition;

    public void Init(UnitInfo info, BattleCells cells)
    {
        capacity = info.capacity;
        capacityFrame.text = capacity.ToString();
        image.sprite = Resources.Load<Sprite>("BattleScene/Units/" + info.sprite);
        SetSpritePosition(image);

        this.cells = cells;
    }

    private static void SetSpritePosition(Image image)
    {
        image.SetNativeSize();
        BattleCell battleCell = Prefabs.Get().battleCell;
        float unitHeight = image.GetComponent<RectTransform>().rect.height;
        float cellSize = battleCell.GetComponent<RectTransform>().rect.height;
        float scale = image.GetComponent<RectTransform>().localScale.y;
        float offset = unitHeight * scale / 2 - cellSize / 2;
        image.GetComponent<RectTransform>().transform.position = new Vector3(0, offset, 0);
    }

    public void Spawn(int x, int y)
    {
        unitPosition = cells.GetCell(x, y);
        transform.localPosition = (Vector3)cells.GetPosition(x, y) + new Vector3(0, 0, -5);
    }
}