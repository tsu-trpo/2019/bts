﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BattleGround : MonoBehaviour
{
    public BattleCells battleCells;

    void Start()
    {
        var rect = GetComponent<RectTransform>();
        battleCells = new BattleCells(rect);
        
        List<UnitInfo> allies = new List<UnitInfo>();
        for (int i = 0; i < 6; i++)
        {
            allies.Add(new UnitInfo(10, "blackdragon"));
        }

        List<UnitInfo> enemies = new List<UnitInfo>();
        for (int i = 0; i < 7; i++)
        {
            enemies.Add(new UnitInfo(10, "greendragon"));
        }

        SpawnUnits(allies, true);
        SpawnUnits(enemies, false);
    }

    void SpawnUnits(List<UnitInfo> units, bool isAllies)
    {
        int x = isAllies ? 0 : battleCells.GetColumnCount() - 2;

        int[] unitsPosition = makeUnitsPosition(units.Count);
        int unitIndex = 0;

        for (int i = 0; i < 7; i++)
        {
            if (unitsPosition[i] == 0)
            {
                continue;
            }

            var unit = Instantiate<Unit>(Prefabs.Get().Unit);

            unit.transform.SetParent(transform);
            unit.Init(units[unitIndex++], battleCells);

            int offset = (i + x) % 2;
            unit.Spawn(x + offset, i);
            if (!isAllies)
            {
                unit.transform.localRotation = Quaternion.Euler(0, 180, 0);
                unit.capacityFrame.transform.localRotation = Quaternion.Euler(0, 180, 0);
            }
        }
    }

    private int[] makeUnitsPosition(int unitsCount)
    {
        int[] unitsPosition;
        switch(unitsCount)
        {
            case 1:
                unitsPosition = new int[7] {0,0,0,1,0,0,0};
                break;
            case 2:
                unitsPosition = new int[7] {0,0,1,0,1,0,0};
                break;
            case 3:
                unitsPosition = new int[7] {0,1,0,1,0,1,0};
                break;
            case 4:
                unitsPosition = new int[7] {1,0,1,0,1,0,1};
                break;
            case 5:
                unitsPosition = new int[7] {0,1,1,1,1,1,0};
                break;
            case 6:
                unitsPosition = new int[7] {1,1,1,0,1,1,1};
                break;
            case 7:
                unitsPosition = new int[7] {1,1,1,1,1,1,1};
                break;
            default:
                unitsPosition = new int[7] {0,0,0,0,0,0,0};
                Debug.LogError("Сработал дефолт");
                break;
        }
        return unitsPosition;
    }
}
