﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleCells : Cells<BattleCell>
{
    public BattleCells(RectTransform field) : base(field, Prefabs.Get().battleCell)
    {
        CreateCells(field);
    }

    public override void CreateCells(RectTransform field)
    {
        CalcMatrixSize(field);

        cells = new BattleCell[columnCount, rowCount];
        int cellCount = rowCount * columnCount;

        for(int i = 0; i < cellCount; i++)
        {
            BattleCell cell = Instantiate<BattleCell>(Prefabs.Get().battleCell);
            
            cell.transform.SetParent(field.transform);
            cell.Init(i % columnCount, i / columnCount);

            cells[cell.x, cell.y] = cell;
            cell.transform.localPosition = GetPosition(cell.x, cell.y);
        }
    }

    private void CalcMatrixSize(RectTransform field)
    {
        int width = (int)field.rect.width;
        int height = (int)field.rect.height;
        
        rowCount = height / cellSize;
        columnCount = width / cellSize;
    }
}
